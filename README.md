
Description
-----------

mk_dist:
creates input read by mp_absreDD.pro and Table 1 in Siebenmorgen &
Chini, https://ui.adsabs.harvard.edu/abs/2023arXiv231103310S/abstract
doi: 10.48550/arXiv.2311.03310


crossec_dd.f:
This file is designed to compute cross sections and optical depth
using the Dark Dust model as described in the publication by
Siebenmorgen R. (2023 A&A, 670A, 115).

mp_absredd.pro:
Use this script for fitting reddening curves with the Dark Dust model,
as illustrated in Figure 3 of the manuscript available at
http://ui.adsabs.harvard.edu/abs/2023arXiv231103310S/.

abunTab2
converts abundanses to specific masses and outputs latex table 2
http://ui.adsabs.harvard.edu/abs/2023arXiv231103310S/.

pl_Fig*:
create pl_Fig*.pdf which is Fig.1, 2, and 3a,b,c, in Siebenmorgen &
Chini, https://ui.adsabs.harvard.edu/abs/2023arXiv231103310S/abstract
doi: 10.48550/arXiv.2311.03310


---------

IDL cookbook:
---------
use IDL 8.8.2 (darwin x86_64 m64) and start idl and type:

!PATH = './AAstron/:' + !PATH
!PATH = './Coyote/:'  + !PATH
!PATH = './MPFIT/:'   + !PATH

; prepare and plot Fig.1
mk_dist 
pl_Fig1 

; verify and copy (if needed): cp mk_dist.xdr ./Input/

; start fitting 
mp_absredd, /ps

; prepare and plot Fig.3
abunTab2
pl_Fig3


; pl_Fig2 use multiplot for some users recommended to restart idl for
;  window settings when pl_Fig3 is sued an d another ps shall be
;  created.

pl_Fig2a
pl_Fig2b
pl_Fig2c

exit
end



