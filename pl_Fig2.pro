Pro pl_Fig2
; plot luminosity distance versus GAIA
; V=dwarfs, IV=subgiants, III=giants, II=bright giants, I=hyper giants
; Comments : 47 stars
; ------
;


!p.font=0
!p.charsize =1.3
!p.thick =2
    window, 0, xsi=500, ysize=600
    a = PSWINDOW(/cm)
    device, decomposed=0
    set_plot, 'PS'
    device, filename='idl.ps',BITS_PER_PIXEL=8, /color,  $
           XSIZE=a.xsize, YSIZE=a.ysize, XOFFSET=a.xoffset, YOFFSET=a.yoffset


restore, /verb, './Input/mk_dist.xdr'
restore, /verb, 'abunTab2.xdr'

nn = n_elements(star)
ds0_mod = ds0 * 10^((Av - vtauv_mod*2.5/alog(10.)) /5.)
ds1_mod = ds1 * 10^((Av - vtauv_mod*2.5/alog(10.)) /5.)



;  --------  Only dwarfs lc =5 V stars  --------
;
ii5    = where(lc eq 5 or lc eq 4, nn5)
ii5_b  = where(lc eq 5 or lc eq 4 and sc ge 10 and sc lt 14,nn5_B ) 
ii5_b4 = where(lc eq 5 or lc eq 4 and sc ge 14,nn5_B4 ) 
ii5_o  = where(lc eq 5 or lc eq 4 and sc lt 10,nn5_O ) 


ii3    = where(lc eq 3, nn3) & print, nn5
ii3_b  = where(lc eq 3 and sc ge 10 and sc lt 14,nn3_B ) 
ii3_b4 = where(lc eq 3 and sc ge 14,nn3_B4 ) 
ii3_o  = where(lc eq 3 and sc lt 10,nn3_O ) 


ii1    = where(lc gt 5, nn1)
ii1_b  = where(lc gt 5 and sc ge 10 and sc lt 14,nn1_B ) 
ii1_b4 = where(lc gt 5 and sc ge 14,nn1_B4 ) 
ii1_o  = where(lc gt 5 and sc lt 10,nn1_O ) 

; check number of stars:
print, '  V stars: ', nn5_o+nn5_b+nn5_b4, nn5
print, 'III stars: ', nn3_o+nn3_b+nn3_b4, nn3
print, '  I stars: ', nn1_o+nn1_b+nn1_b4, nn1

print,' all stars: ', nn5+nn3+nn1, nn

; ------------------------------
; DL from model

plot_oo, dg, ds_mod, /nodata, $
      xr=[100, 3500.], yr=[100., 3500], /xsty,/ysty, xthick=4, ythick=4, $
      xtitle = 'D!DGAIA!N (pc)', ytitle = 'D!DL!N (pc)'

;polyfill, [0.,0.,2500., 2500.], [1, 1.2, 1.2, 1], 

oplot, dg, ds,psym=plotsym(/diamond, /fill, scale=1.3, color=fsc_color('gray'))
oplot, dg, ds,psym=plotsym(/diamond,  scale=1.)




oplot, [100., 3500],[100., 3500], thick=1,color=fsc_color('light gray')
ii = where( (abs(ds_mod/dg-1.)) gt -0.05, nb)
for i =0, nb-1 do oplot, [dg(ii(i)), dg(ii(i))], [ds0_mod(ii(i)), ds1_mod(ii(i))], thick=2, Color=FSC_Color('gray')

oploterror, dg, ds_mod, edg, ds_mod*0., psym=3, /nohat, thick=2, color=fsc_color('gray')

;oplot, dg, ds_mod, psym=plotsym(/diamond, /fill, scale=1.,color=fsc_color('light gray'))
;oplot, dg, ds_mod, psym=plotsym(/diamond, scale=1.), thick=4

oplot, dg(ii1_b),  ds_mod(ii1_b),  psym=plotsym(/box, /fill, scale=1.5, color=fsc_color('blue'))
oplot, dg(ii1_o),  ds_mod(ii1_o),  psym=plotsym(/box, /fill, scale=1.5, color=fsc_color('orange'))



oplot, dg(ii3_b),  ds_mod(ii3_b),  psym=plotsym(/circle, /fill, scale=1.5, color=fsc_color('blue'))
oplot, dg(ii3_o),  ds_mod(ii3_o),  psym=plotsym(/circle, /fill, scale=1.5, color=fsc_color('orange'))
oplot, dg(ii3_b4), ds_mod(ii3_b4), psym=plotsym(/circle, /fill, scale=1.5, color=fsc_color('green'))

oplot, dg(ii5_b),  ds_mod(ii5_b),  psym=plotsym(/superstar, /fill, scale=1.5, color=fsc_color('blue'))
oplot, dg(ii5_o),  ds_mod(ii5_o),  psym=plotsym(/superstar, /fill, scale=1.5, color=fsc_color('orange'))
oplot, dg(ii5_b4), ds_mod(ii5_b4), psym=plotsym(/superstar, /fill, scale=1.5, color=fsc_color('green'))

oplot, dg(ii1),    ds_mod(ii1),    psym=plotsym(/box, scale=1.5)
oplot, dg(ii3),    ds_mod(ii3),    psym=plotsym(/circle, scale=1.5)
oplot, dg(ii5),    ds_mod(ii5),    psym=plotsym(/superstar, scale=1.5)


legend, ['O4  -  O9', 'B0  -  B3', 'B4  -  A0'], charsize=1.5, charthick=6, box=1, $
        textcolor=[fsc_color('orange'), fsc_color('blue'), fsc_color('green')]

oplot, [1350.,1350], [178,178], psym=plotsym(/diamond, /fill, scale=0.9,  color=fsc_color('gray'))
oplot, [1350.,1350], [178,178], psym=plotsym(/diamond, scale=0.9)
oplot, [1350.,1350], [160,160], psym=plotsym(/superstar, scale=0.9)
oplot, [1350.,1350], [138,138], psym=plotsym(/circle, scale=0.9)
oplot, [1350.,1350], [120,120], psym=plotsym(/box, scale=0.9)

x=5.
xyouts, 1455, 177 -x, 'Fig.1'      , charsize=0.95
xyouts, 1455, 159 -x, 'Dwarfs V, IV'      , charsize=0.95
xyouts, 1455, 138 -x, 'Giants III'    , charsize=0.95
xyouts, 1455, 120 -x, 'Supergiants I' , charsize=0.95


plot_oo, dg, ds_mod, /nodata, /noerase, $
      xr=[100, 3500.], yr=[100., 3500], /xsty,/ysty, xthick=4, ythick=4, $
      xtitle = 'D!DGAIA!N (pc)', ytitle = 'D!DL!N (pc)'


; --------------

   device, /close
   set_plot, 'X'
   device, decomposed=1
   print
   print, ' Result of reddening fit in plot :  pl_Fig2.pdf'
   com =  ' ps2pdf                  idl.ps     pl_Fig2.pdf'
spawn, com
print, com + '     ->      DONE'

end
