Pro pl_Fig2a

; For 1- 15 
; pLot the absolute reddening with dark dust model fit
; From Extin Paper: Rv_ref, Av_ref, Ebv_ref
; In   HD*para.xdr:
; --------------------------------------------------
;


!p.font=0
!p.charsize =1.3
!p.thick =2
    window, 0, xsi=500, ysize=600
    a = PSWINDOW(/cm)
    device, decomposed=0
    set_plot, 'PS'
    device, filename='idl.ps',BITS_PER_PIXEL=8, /color,  $
           XSIZE=a.xsize, YSIZE=a.ysize, XOFFSET=a.xoffset, YOFFSET=a.yoffset

   multiplot, [4,5]


;
; /Users/rsiebenm/DATA/Dist/mk_dist.zdr
  restore, /verb, './Input/mk_dist.xdr'
  restore, /verb, 'anaRedd.xdr'
   vrv_ref  = rv                ; Rv  of Extinction paper
   aV_ref   = av   > 0.001      ; Av  of Extinction paper
   delvar, av, rv

; ---------------------------
;
   
nn    = n_elements(star)

for i = 0, 15 do begin

   target  = star(i)

 print, ' plot target = ', target
      
; read data: absolute reddening curve ---------
  fxdr = findfile('./Result/'+target+'_para.xdr',   cou=nfx)
  if nfx ne 1 then  stop, 'check files =', fxdr
  
  restore, fxdr(0)
   ebv_ref = av_ref(i) /vrv_ref(i)    ; Ebv of Extinction paper
   model  = reddpolfit(0:nr-1)
   data   =  data(0:nr-1)   * ebv_ref
   edata  = edata(0:nr-1)   * ebv_ref
   wdata  = wdata(0:nr-1)
   tauV   = vtauv(i)
; check that -tauV = E(oo)<E(H,K), Eq.3 in SC2023 is hold, for this read
; the relative reddening curve of the star and converts to absolute
; reddening using Ebv of reddening curve paper: 
   if (-vtauv(i)*1.086) ge data(2) then $
       vtauV(i)  = -data(2)/(2.5/alog(10.))
;     ensure E(oo)<E(K), for:
   if (target eq 'HD070614' or target eq 'HD110946' or target eq 'HD287150') then $
       vtauV(i)  = -data(1)/(2.5/alog(10.))
       tauV      = vtauV(i)
       data(0)   = -2.5/ alog(10.) * tauV
       edata(0)  = abs(data(0)) * 0.1
       
; read DD model: result ---------
; Kappa.out
       fkap = findfile('./Result/'+target+'_Kappa.out', cou=nfk)
       if nfk ne 1 then  stop, 'check files =', fkap
       
       rdfloat, fkap(0), skipline=3, w, sac, ssac, sasi, $
        sssi, savgr, ssvgr, savsi, ssvsi, sapah, sad, ssd, st, /sil
  w   = w*1e4
  iv  = (where(w le 0.548))(0) & ib  = (where(w le 0.445))(0)
  Rv_mod      = st(iv) / (st(ib) - st(iv))
 Ebv_mod      = 2.5/alog(10.) * (st(ib) - st(iv))
  Av_mod      = Rv_mod * Ebv_mod
 absredd_mod  = 2.5/alog(10.) * (st/st(iv) -1.) * st(iv) 


; plot reddenig for star:
 
    if (i le 11) then  xtit = [' ', ' ', ' ', ' ', ' ', ' '] 
    if (i gt 11) then  xtit = [' ', '2', '4', '6', '8', '10']
    
    ytit = [' ', ' ' , ' ', ' ', ' ', ' ' , ' ', ' ']

    if (i eq 0 or i eq 4 or i eq 8 or i eq 12 or i eq 16) then  $
        ytit = ['-4', ' ' , ' 0', ' ', ' 4', ' ' , ' 8', ' '] 

  yrange = [min(data-edata)-0.2, max(data+edata)+0.2]  
 plot, 1./w, absredd_mod, xrange=[-0.1,11.1], yrange=[-4,10], /xsty, /ysty, $
       xthick=2, ythick=2, /nodata, xtickname=xtit, ytickname=ytit

 oplot, 1./w, ((sac+ssac + sasi+sssi)/st(iv)   -1.)* Av_mod, color=fsc_color('blue')
 oplot, 1./w, ((savsi+ssvsi+ sapah+savgr+ssvgr)/st(iv) -1.)* Av_mod, color=fsc_color('forest green')
 if vdark_mass(i) gt 0.01 then oplot, 1./w, ((sad+ssd)/st(iv)     -1.)            * Av_mod, thick=!P.thick+1, color=fsc_color('magenta')

 oplot, 1./w, absredd_mod, thick=3
; data
 oploterror, 1./wdata(1:*), data(1:*), wdata(1:*)*0, 0.1*abs(data(1:*)) > 0.05, /nohat, $
             color=fsc_color('dark gray'), psym=3
 oplot, 1./wdata(0:*), data(0:*),  psym=plotsym(/circle,scale=0.4,color=fsc_color('light gray'), /fill)
 oplot, 1./wdata(0:*), data(0:*),  psym=plotsym(/circle,scale=0.4)
 oplot, 1./w, absredd_mod, thick=2
 xyouts, 1, 7, target, charsize = 0.85

multiplot
 
endfor

     xtitle  = '!9l!3!U-1!N (!9m!3m!U-1!N)'
     ytitle = '!3E(!9l!3-V) (mag)'
   xyouts, 0.55, 0.18, xtitle, charsize = 1.2, /norm, align=0.5
   xyouts, 0.07, 0.62, ytitle, charsize = 1.3, /norm, orient=90, align =0.5



   device, /close
   set_plot, 'X'
   device, decomposed=1


   spawn, 'ps2pdf idl.ps pl_Fig2a.pdf'
   print
   print, '            pl_Fig2a.pdf --- DONE '
   print
   
end
