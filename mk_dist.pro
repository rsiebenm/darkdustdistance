Pro mk_dist
;
; IDL Program: mp_dist - computes tauV (Eq.3) for unifying distances and
;                        creates Table 1 as in Siebenmorgen & Chini,
;                        https://ui.adsabs.harvard.edu/abs/2023arXiv231103310S/abstract
;                        doi: 10.48550/arXiv.2311.03310
;  
;     INPUT:
;            read from directory ./Input/:
; GAIAdr3_110.csv    : GAIA Collaboration 2023, A&A, 674, A41
; PhotoSpTRvAv.tab   : Siebenmorgen et al. 2023,  A&A 676A, 132
; Bowen08_Tab11.txt  : Table 11 in Bowen et al 2008, 176, 59-163
; Wegner07_Tab8.txt  : Table 8 in Wgner et al., 2007, MNRAS 374, 1549-1556
;
;            read from directory ./StartParameters/

; fxdr               : idl saveset in file fxdr, which include the
;                      relative reddening curve from refernece papwr
;                      as in Siebenmorgen et al. 2023, A&A 676A, 132
;                      
; 
;  Output: in directory ./
; mk_dist.xdr        : idl save set
; mk_dist.tab        : ascii 
; Tab1.tex           : Table 1 (simillar as mk_dist.tab) in latex 
; fxdr               : updated data(0) = -2.5/ alog(10.) * tauV
;                      and save in ./StartParameters/
; -----------------------------------------------------------------
;


; output:
get_lun, iu
openw, iu, 'mk_dist.tab'
get_lun, iut
openw, iut, 'Tab1.tex'

; ----------  reading files:
fmt='(i,a,f,f,f,a,a,a)'
readcol, './Input/PhotoSpTRvAv.tab', format=fmt, skipl=2, $
         lc, star, Vk, vRv, vAv, ref, fsm, spt



 nn     = n_elements(star)
 Mv     = fltarr(nn) -9.
 dMvs0  = fltarr(nn) -9.
 dMvs1  = fltarr(nn) -9.
 dMvl0  = fltarr(nn) -9.
 dMvl1  = fltarr(nn) -9.

 dMv0   = fltarr(nn) -9.
 dMv1   = fltarr(nn) -9.
   

 DB    = fltarr(nn) -9.
eDB    = fltarr(nn) -9.
 DW    = fltarr(nn) -9.
eDW    = fltarr(nn) -9. 
 DG    = fltarr(nn) -9.
eDG    = fltarr(nn) -9.

 vtauV = fltarr(nn) -9.
evtauV = fltarr(nn) -9.

; ======================================
; GAIA:
r3     = read_csv('./Input/GAIAdr3_110.csv')
p3     = r3.FIELD01
ep3    = r3.FIELD02
g_snr3 = r3.FIELD03
g3     = r3.FIELD04
bp_g3  = r3.FIELD05
star3  = r3.FIELD06
ii = where(p3 eq 0)
p3(ii)  = -1.
ep3(ii) = 1e99
d3      = 1000./p3  ; pc
ed3     = d3 * (ep3/p3)

; ======================================
; Absolute magnitudes Mv, SpT  and luminosity class codes
sc      = float(strmid(spt,1,3))
iib     = where(strmid(spt,0,1) eq 'B')
iia     = where(strmid(spt,0,1) eq 'A')
sc(iib) = sc(iib) + 10.
sc(iia) = sc(iia) + 20.


iil5 = where(lc eq 5, nn5)
iil4 = where(lc eq 4, nn4)
iil3 = where(lc eq 3, nn3)
nn2  = 0  ; no II types in sample
iil11 = where(lc eq 11, nn11)
iil12 = where(lc eq 12, nn12)
if (nn11+ nn12+ nn2+ nn3+ nn4+ nn5 ne nn) then stop, 'check lum class', spt(iis2)

; -- abs magnitude Mv:

fmt='(a,f,f,f,f,f,f,f,f)'

readcol, './Input/Bowen08_Tab11.txt',  skipl=2, format=fmt, $
          sb, bcode, bv, biv, biii, bii, bib, biab, bia


readcol, './Input/Wegner07_Tab8.txt', skipl=2, format=fmt, $
         sw, wcode, wv, wiv, wiii, wii, wib, wiab, wia



; Input below corrects Mv for binary stars  '
star_bin      = ['HD046149', 'HD093205', 'HD156247', 'HD167264', 'HD167771']
spt_bin2      = ['+B0.5V', '+O8V',       'B5V',  '+B0V', '+O8III']
Mv_bin2       = [-3.55,    -4.6,         -1.21,   -3.85,   -5.5]
dmvs0_bin2    = [0.15,     0.075,         0.02,    0.15,    0.05]         ; abs(spt-0.5 - spt)
dmvs1_bin2    = [0.255,    0.075,         0.08,    0.15,    0.05]         ; abs(spt+0.5 - spt_
dmvl0_bin2    = [0.325,    0.22,         0.135,    0.295,   0.23]         ; lc-1 -lc iii-iV
dmvl1_bin2    = [0.325,    0.22,         0.135,    0.295,   0.185]        ; lc+1 -lc iii-ii
dmc_bin2      = [0.056,    0.056,        0.056,    0.056,   0.25]


; In our sample (Siebenmorgen et al 2023, Tab.4)
; there are no lc>10 sources for sc >13
; there are no lc=4  sources for sc <13
; there are no lc=2  sources 

for i =0, nn-1 do begin

;     Bowen08   
   if sc(i) le 13. then begin
   ii   = (where(bcode eq sc(i), nnb))(0)  
   iis0 = (where(bcode eq (sc(i)-0.5) > 3, nn0))(0)
   iis1 = (where(bcode eq (sc(i)+0.5) < 13.5,nn1))(0)
   if nnb ne 1 or nn0 ne 1 or nn1 ne 1 then stop, ' check Bowen nn ne 1'
   
   if lc(i) eq  5. then begin
      mv(i)  =      bv(ii)
      dmvl1(i) = abs(bv(ii)   - biv(ii)) 
      dmvl0(i) = dmvl1(i)

      dmvs0(i) = abs(bv(iis0) - bv(ii))
      dmvs1(i) = abs(bv(iis1) - bv(ii))
;      print, i,  dmvs1(i), abs(bv(iis1) - bv(ii)) , iis1, ii
;      stop, star(i), '  ' , spt(i)
   endif

   if lc(i) eq  3. then begin
      mv(i)    =     biii(ii)
      dmvl0(i) = abs(biii(ii)   - biv(ii))  
      dmvl1(i) = abs(biii(ii)   - bii(ii))  
      dmvs0(i) = abs(biii(iis0) - biii(ii))
      dmvs1(i) = abs(biii(iis1) - biii(ii))
   endif

   if lc(i) eq  11. then begin
      mv(i)    =     bia(ii)
      dmvl0(i) = abs(bia(ii)   - bib(ii))
      dmvl1(i) = dmvl0(i) ; 0
      dmvs0(i) = abs(bia(iis0) - bia(ii))
      dmvs1(i) = abs(bia(iis1) - bia(ii))
   endif

   if lc(i) eq  12. then begin
      mv(i)    =     bib(ii)
      dmvl0(i) = abs(bib(ii)   - bii(ii)) 
      dmvl1(i) = abs(bib(ii)   - bia(ii)) 
      dmvs0(i) = abs(bib(iis0) - bib(ii))
      dmvs1(i) = abs(bib(iis1) - bib(ii))
   endif

;   print,i, ' B ', lc(i), sc(i), mv(i), dmvl0(i), dmvl1(i), dmvs0(i), dmvs1(i)
   
endif else begin
;     Wegner07   
   ii  = (where(wcode eq sc(i), nnw))(0)
   iis0 = (where(wcode eq (sc(i)-0.5) > 13.5, nn0))(0)
   iis1 = (where(wcode eq (sc(i)+0.5) < 29, nn1))(0)


   if nnw ne 1 or nn0 ne 1 or nn1 ne 1 then stop, ' check Wegner nn ne 1'


   if lc(i) eq  5.  then begin
      mv(i)   =     wv(ii)
      dmvl1(i) = (abs(wv(ii)   - wiii(ii))) /2.
      dmvl0(i) = dmvl1(i)  
      dmvs0(i) = abs(wv(iis0) - wv(ii))
      dmvs1(i) = abs(wv(iis1) - wv(ii))
;      if sc(i) eq 15 then
;      print,   i, ' W ', star(i), ' ', spt(i), lc(i), sc(i), mv(i), dmvl0(i), dmvl1(i), dmvs0(i), dmvs1(i), ii, iis0, iis1
;      stop
      
      
   endif

   if lc(i) eq  4. then begin
      mv(i)    =     wiv(ii)
      dmvl1(i) = (abs(wv(ii)   - wiii(ii))) /2.
      dmvl0(i) = dmvl1(i)  
 
      dmvs0(i) = abs(wiv(iis0) - wiv(ii))
      dmvs1(i) = abs(wiv(iis1) - wiv(ii))

   endif

   if lc(i) eq  3. then begin
      mv(i)    =     wiii(ii)
      dmvl0(i) = abs(wiii(ii)   - wiv(ii)) 
      dmvl1(i) = abs(wiii(ii)   - wii(ii))   
      dmvs0(i) = abs(wiii(iis0) - wiii(ii))
      dmvs1(i) = abs(wiii(iis1) - wiii(ii))

;      if sc(i) eq 19 then $
;      print,   i, ' W ', star(i), ' ', spt(i), lc(i), sc(i), mv(i), dmvl0(i), dmvl1(i), dmvs0(i), dmvs1(i), ii, iis0, iis1
;      stop

      
   endif

   
; print,i, 'W', lc(i), sc(i), mv(i), dmvl0(i), dmvl1(i), dmvs0(i), dmvs1(i)
   endelse

; ----------------------   
; GAIA distance   
   iig  = where(star3 eq star(i), ng)
   if ng lt 1 then stop, ' check GAIA: no entry   for star = ', star(i)
   if ng gt 1 then stop, ' check GAIA   2 entries for star = ', star(i)
   dg(i) = d3(iig(0))  & edg(i) =  ed3(iig(0))


endfor


; Eq. B10 in Bowen2008, mit dmC = literature uncertainty in Mv
; relpace varians Delta by Error sigma:=Delta/2.
; 
min_mv =  0.                    
dmvl0 = (dmvl0 > min_mv) / 2.
dmvl1 = (dmvl1 > min_mv) / 2.
dmvs0 = (dmvs0 > min_mv) / 2.
dmvs1 = (dmvs1 > min_mv) / 2.


; add offset in Mv for giants betwen Wegner and Bowen of 0.25
dmC       = lc*0. 
ii5       = where(lc eq 5 )
ii3       = where(lc eq 3 and sc gt 13)
ii11      = where(lc eq 11 )
ii12      = where(lc eq 12 )

dmC(ii5)   = 0.056
dmC(ii3)   = 0.25
dmC(ii11)  = 0.22
dmC(ii12)  = 0.18

dmv0  = sqrt(dmvl0^2 + dmvs0^2 + dmC^2) 
dmv1  = sqrt(dmvl1^2 + dmvs1^2 + dmC^2)


; correct for Mv + err for binary stars
for i = 0, nn-1 do begin
  ii  = where(star(i) eq star_bin, nbin) & ii = ii(0)
 if nbin eq 1 then begin
    spt(i) = spt(i) + spt_bin2(ii)
    mv(i) = -2.5 * alog10 (10^(-0.4*mv(i)) + 10^(-0.4*mv_bin2(ii)))
   dmv0(i) = sqrt(dmv0(i)^2 + dmvs0_bin2(ii)^2 + dmvl0_bin2(ii)^2 + dmc_bin2(ii)^2)
   dmv1(i) = sqrt(dmv1(i)^2 + dmvs1_bin2(ii)^2 + dmvl1_bin2(ii)^2 + dmc_bin2(ii)^2)
 endif
endfor




; ---------------------------------------------------
; Luminosity distance:
; Av = ebv *Rv 
; mv - Mv = 5 logD - 5   + Av 
; D = 10.^((5. + mv -Mv  -Av)/5.)


ds    = dg *0. -9.99
ds1   = dg *0. -9.99
ds0   = dg *0. -9.99

ds    = 10.^((5. + Vk - Mv  - vAv)/5.)
ds0   = 10.^((5. + Vk - Mv -dMv0 - vAv)/5.) ; kleine Entfernung
ds1   = 10.^((5. + Vk - Mv +dMv1 - vAv)/5.) ; grosse Entfernung


eMv = Mv *0. -9.99


fmt  = '(a8,a12,2i4,2a3, 4f7.2, 1x, 3f8.1,2x, f8.1,f6.1, f7.2,f6.2)'
print, '# Star            SpT  sc  lc SM Ref  Vk     Av     Rv     Mv     Dspt    Dspt0   Dsp1      DG     eDG    tauV  eTauv'
printf, iu, '# Star            SpT  sc  lc SM Ref  Vk     Av     Rv     Mv     Dspt    Dspt0   Dsp1      DG     eDG    tauV  eTauv'


 for i          = 0, nn-1 do begin   
    vtauV(i)    =  alog(10.)/2.5 * (Vk(i) -Mv(i) +5. - 5.*alog10(DG(i)))
   evtauV(i)    =  alog(10.)/2.5 * sqrt(( (dMv0(i)+dMv1(i))/2.  )^2. + (5./alog(10.) * eDG(i)/DG(i))^2.)
  
  fxdr           = findfile('./StartParameters/'+star(i)+'_para.xdr',   cou=nf)
  restore, fxdr
    data         = data(0:nr-1)   * ebv
   edata         = edata(0:nr-1)  * ebv
; check that -tauV = E(oo)<E(H,K), Eq.3 in SC2023 is hold, for this read
; the relative reddening curve of the star and converts to absolute
; reddening using Ebv of reddening curve paper: 
   if (-vtauv(i)*1.086) ge data(2) then $
       vtauV(i)  = -data(2)/(2.5/alog(10.))
;     ensure E(oo)<E(K), for:
   if (target eq 'HD070614' or target eq 'HD110946' or target eq 'HD287150') then $
       vtauV(i)  = -data(1)/(2.5/alog(10.))
       tauV      = vtauV(i)
       data(0)   = -2.5/ alog(10.) * tauV
       edata(0)  = abs(data(0)) * 0.1
   
; save relative reddening by dividing with Ebv and tauV, Rv
       data      =  data/ebv
      edata      = edata/ebv       
  save, /xdr, filename=fxdr, target, redchi2, redchi2p, $ 
      abuc, abusi,  abuvsi,    abucvgr,    abucpahs,  qmrn, alec, alesi, aled, $
      arad_polmin_aC, arad_polmin_Si, arad_polmax, x0, gam, $
      Rv, Rv_obs, tauV, Ebv, $
      rmsabuc, rmsabuSi, rmsabuvsi, rmsabucvgr, rmsabucpahs, rmsqmrn, $
      rmsalec, rmsalesi, rmsarad_polmin_aC,rmsarad_polmin_Si,  rmaC, rmSi, rmaCalig,  $
      rmSialig, rmDark, rmsX0, rmsGam, $
      wdata, data, edata, edata_norm, reddpolfit, nr, np


printf, iu, format= fmt, star(i), spt(i), sc(i), lc(i), fsm(i), ref(i), Vk(i), vav(i), vrv(i), $
           Mv(i), Ds(i), Ds0(i), Ds1(i), DG(i), eDG(i),vtauV(i),evtauV(i)
   
endfor

; ==================== Output 
av = vav
rv = vrv

save, /xdr, filename='mk_dist.xdr', star, spt, sc, lc, fsm, ref, $
      vk, av, rv, Mv, eMv, Ds, Ds0, Ds1, DG, eDG, vtauV,evtauV, $
      dmv0, dmvl0, dmvs0, dmv1, dmvl1, dmvs1, dmc


; - Table 1 as latex table in file Tab1.tex
Av_mod     = vtauV * 2.5/alog(10)
Ds_mod     = 10.^((5. + Vk - Mv - Av_mod )/ 5.)
fmtex    = '(a8, a3, a12, a4, f5.2,  a5, f5.2, a5, f5.2, a5, f5.2, a3, a3, a3, f5.2, a3, i5, a6, i4, a5, i5, a6, i5, a5, i4, a4, i4, a3, f5.2, a3)'                             
for i =0, nn -1 do  begin
   printf, iut, '\smallskip'
   printf, iut, format=      fmtex, star(i), ' & ', spt(i), '& $', Mv(i),'_{', -dmv0(i), '} ^{', dmv1(i), '}$ & ', $
   Vk(i), ' & ', ref(i),' & ', av(i),   '&$', nint(Ds(i)), '_{', nint(ds0(i)-ds(i)),    $
          '}^{', nint(Ds1(i)-ds(i)), '}$ & $', nint(DG(i)), '\pm ', nint(eDG(i)), '$ & ', nint(DS_mod(i)), ' & ', Av_mod(i), ' \\'
endfor



close, iu
close, iut
free_lun, iu
free_lun, iut

; =================================

print
print, ' mk_dist: DONE  results  saved:   mk_dist.xdr, mk_dist.tab, Tab1.tex'
print


end

