Pro abunTab2
; check abundance constrain Eq.1 in Siebenmorgen (2023): abundances:
;          [Si]/[C] < 5.2, Hensley&Draine21: [Si]/[C] = 3.5+/-3*0.6
;
; creates Tab2 and distance of the Dark dust model ds_mod.
;
; Output:
;   abunTab2.xdr, Tab2.tex
;
; Notation:
;       *_ref : literature value as in redd papers
;       *_obs : Model with strikt Dgaia=Dspt (ignoring E(oo) < E(H))
;       *_mod : Model Eq.3
; -------------


; Input:
restore,      './Input/mk_dist.xdr'
file  = findfile('./Result/HD*.xdr', cou=nf)


; output latex file:
get_lun, iut
openw, iut, 'Tab2.tex'


vEbv_ref      = Av/Rv        ;  as given in references of the reddening curves
vtauv_obs     = vtauV


vchi2         = fltarr(nf)
vRv_ref       = fltarr(nf)   ; Rv literature value as in redd papers
vRv_obs       = fltarr(nf)   ; what DD model should use for Dgaia=Dspt
vRv_mod       = fltarr(nf)   ; what DD model used  folowing Eq. 3
vEbv_mod      = fltarr(nf)   ; reddening used in DD model 
vTauv_mod     = fltarr(nf)   ; tauV      used in DD odel 
vabuc         = fltarr(nf)
vcsi          = fltarr(nf)      ; total [Si/[C] in large + dark dust
vdl           = fltarr(nf)
vnd           = fltarr(nf)
vnl           = fltarr(nf)

; abundances Dark dust abuc/abusi =
abud_aCSi  = 2.815


for i         = 0, nf -1 do begin
   restore,     file(i)
   if target ne star(i)   then stop, ' check star name: ', i, target, '  ', star(i)
   
   vchi2(i)     = REDCHI2
   vabuc(i)     = abuc
   vcsi(i)      = (ndust_l/(ndust_l+ndust_d)* (abuc+abucpahs+abucvgr) / (abuvsi + 15.)) + (ndust_d/(ndust_l+ndust_d) * abud_aCSi ) 
   vnd(i)       = ndust_d 
   vnl(i)       = ndust_l
   vdl(i)       = ratio_dl
   
   vRv_ref(i)   = Rv_ref
   vRv_obs(i)   = Rv_obs
   vRv_mod(i)   = Rv_mod
   vEbv_mod(i)  = Ebv_mod
   vTauV_mod(i) = Rv_mod * ebv_mod  / 2.5 * alog(10.) ; to convert from Av into tau

   

; check abundance constarint   
   if     vcsi(i)    gt 5.2    then $
   print, vcsi(i),'  gt 5.2  : check abudance cionstraint --' , star(i)


endfor

Ds_mod = 10.^((5. + Vk - Mv - vtauv_mod*2.5/alog(10.) )/ 5.)


save, /xdr, filename='abunTab2.xdr', star, vchi2, vabuc, vcsi, vnd, vnl, vdl, $
  vEbv_ref, vEbv_mod, vRv_ref, vRv_obs, vRv_mod, vtauV_obs, vtauV_mod, Ds_mod     

; print, ' Done result on file = abunTab2.xdr'
 print, ' ---------------------------------- '

; make latex file of  Table 2: Tab2.tex


wmolc      = 12.0
wmolsi     = 95.2
wmolvsi    = 134.5
abucpahb   = 0.

fmt='(a10,a3, f5.1, a3, f5.1, a3, f5.1, a3, f5.1, a3, f5.1, a3, f5.1, a3, f5.2, a3, f5.2, a3, f5.2,a2, f6.1, a3, i4,a3,i4, a3, f4.1)'

file       = findfile('./Result/HD*.xdr', cou=nf)

for i      = 0, nf -1 do begin
   restore,     file(i)

       abuc_tot = abuc + abucvgr + abucpahs + abucpahb

       abuwmol  = abuc_tot*wmolc + abusi*wmolsi  + abuvsi*wmolvsi  
       aCmass   = abuc     * wmolc  /abuwmol
       pahmasss = abucpahs * wmolc  /abuwmol
       pahmassb = abucpahb * wmolc  /abuwmol
       vgrmass  = abucvgr  * wmolc  /abuwmol
       Simass   = abusi    * wmolsi /abuwmol
       vsimass  = abuvsi   * wmolvsi/abuwmol
       tmassMRN =  Simass+ aCmass+ vgrmass+vsimass+ (pahmasss+pahmassb)
       if abs(tmassMRN-1.) gt 1.e-3 then stop, ' wrong tmassMRN', tmassMRN

        mrn_mass = ndust_l /(ndust_l  + ndust_d)
       dark_mass = ndust_d /(ndust_l  + ndust_d)
       
       aCmass   = aCmass   * mrn_mass
       pahmass  = pahmasss  * mrn_mass
       vgrmass  = vgrmass  * mrn_mass
       Simass   = Simass   * mrn_mass
       vsimass  = vsimass  * mrn_mass
       tmassMRN = Simass+ aCmass+ vgrmass+vsimass+ pahmass
       if abs(tmassMRN-mrn_mass) gt 1.e-3 then $
          stop, ' wrong mrn_mass', tmassMRN
       if abs(tmassMRN+dark_mass-1.) gt 1.e-3 then $
          stop, ' wrong tmassMRN', dar_mass, tmassMRN 
             
printf, iut, format= fmt, star(i), ' & ', 100*dark_mass, ' & ', 100*Simass, ' & ', 100*vsimass, ' & ', $
           100* aCmass, ' & ', 100*vgrmass, ' & ', 100*PAHmass, ' & ', x0, ' & ', gam, ' & ', qmrn, ' & ', $
           aled*1e4, ' & ', nint(alesi*1e7), ' & ', nint(alec*1e7), '  & ',  redchi2

endfor
close, iut
free_lun, iut

print, ' Done result on files:  abunTab2.xdr and Tab2.tex'

end
