; README of mp_absredd, ps=ps, no_mpfit=no_mpfit
;  
; ----------------------------------------------------------------- 
; IDL Program: mp_absredd - Fits absolute Reddening curve with
;                           Cross-section of Dark Dust (DD) Model
;
; Externals:
;   a)   ./MPFIT/mpfit.pro : MPFIT IDL fitting package
;   b.i)  fkt_absreddRad   : External function of mpfit() -- radii free
;   b.ii) fkt_absredd      : External function of mpfit() -- radii fixed
;   c)   ./a.a.crossec_dd  : Executable of crossec_dd.f (called by fkt_absredd*pro)
;
; INPUT:
;   1) File './StartParameters/mk_dist.xdr', which includes:
;      Name of the stars, and Rv_ref, Av_ref, Ebv_ref from the reddening curves
;      inspected by Siebenmorgen et al,2023, A&A, 676, 132.
;
;   2) Files:
;      2.1) ./StartParameters/HD129557_PAH2170.wq
;           Drude Parameters x0, gam for the 2175AA bump used for
;           computing PAH cross sections. Use parameters in the second
;           line, which are derived experimenting with a.crossec_dd (first line
;           contains parameters derived from spline fits of the reddening curve paper).
;
;      2.2) ./StartParameters/HD112954_jsm12fit.inp
;           Start (12) parameters of dust model read by a.crossec_dd when fitting the reddening curve. One may
;           use default parameters as in crossec_dd.f, however, mpfit convergence is 
;           accelerated significantly when using 'HD*jsm12fit.inp'.
;
;      2.3) ./StartParameters/HD*para.xdr
;           IDL saveset includes previous parameters (2.1 and 2.2) as
;           well as the relative reddening curve E(w-V)/E(B-V) of the
;           star that shall be fit. (These are variables:
;           wdata(0:nr-1), data(0:nr-1), with rms error edata(0:nr-1). Furthermore,
;           Rv_obs, E(B-V)=ebv of the reddening curve paper and
;           Rv=Rv_mod of the Dark Dust model fit (returned by a.crossec_dd)
;           and reduced chi2 (divided by dot).
;
; OUTPUT:
;   './Result/'+ target+'_Kappa.out'    : Abs, sca cross sections K
;   './Result/'+ target+'_PolKappa.out' : Polarization
;   './Result/'+ target+'_PAH2170.wq'   : Update of 2.1
;   './Result/'+ target+'_jsm12fit.inp' : Update of 2.2
;   './Result/'+ target+'_para.xdr'     : Update of 2.3
;   './Result/'+ target+'_message.out'  : Messages of final a.crossec_dd 
;   idl.ps : PS file showing reddening curves (data and dark dust fit)
;
; Keywords:
;   ps: plot reddening curve and fit to pl_allRedd.pdf
;   no_mpfit: 1=nofit 0=mpfit is used (default)
;
; Tips:
;   a) In case you wish to fit a specific star, uncomment the target name setting in
;      the " for i = 0, nn-1 do begin" loop below.
;   b) Parallelization is useful when starting fitting a larger sample.
;      The vectorization of the code can be done by separating the
;      number of stars into different threads. This shall be adjusted to
;      your number of available IDL licenses.
;
; Example: in IDL enter:
;   !PATH = './AAstron/:' + !PATH
;   !PATH = './Coyote/:'  + !PATH
;   !PATH = './MPFIT/:'  + !PATH
;   mp_absredd, /ps
;
; ---------------------------------------------------
;


; ===================================================================================================
Function fkt_absredd, wdata, para

; help function for mpfit, 9 fit parameters of dark dust (DD) model: 
; 0     1       2        3         4     5     6     7               8
; abuc, abuvsi, abucvgr, abucpahs, qmrn alec, alesi, arad_polmin_aC, arad_polmin_Si
; 1) For alec,alesi, arad_polmin* fixed parameters
; 2) For alec,alesi, arad_polmin* feee  parameters (interpol.)
; ------------------------------------------
;
; Get parain, nr,np for Check which parameter is free or constant and
; nr, np of ext and pol data are corect:
restore, './Input/tmp_parain.xdr'

; Polarisation data
;   readcol, './Data/poldata.tab',  wpd, Int, dInt, Qint, dQint, nQ, $
;         Uint, dUint, nU, Pd, epd,theta, dtheta, skip=0, /sil
;     npd = n_elements(pd)   

;
; reads previous parameters: alec and alesi
readcol, './Input/jsm12fit.inp', /sil, skipline=1, numli=1,  $
         abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, alec,  alesi, arad_polmin_aC, arad_polmin_Si, arad_polmax, arad_Darkmax 

abusi          = abusi(0) *1.d0
para           = para     *1d0
abuc           = para(0) 
abuvsi         = para(1) 
abucvgr        = para(2) 
abucpahs       = para(3) 
qmrn           = para(4)
alec           = para(5) 
alesi          = para(6)
arad_polmin_aC = para(7) < alec/1.05
arad_polmin_Si = para(8) < alesi/1.05


   x0             =   para(9)
   gam            =   para(10)   



; use org Drude parameters not those form redd fit !
   get_lun, iu
   openw,iu,'./Input/PAH2170.wq'
   printf, iu, 'x0 (mu)    gam : Drude Parameters at 2175AA bump as in reddening papers'
   printf,iu, x0, gam          ; used here considerung PAH 
   close, iu
   free_lun, iu


   
arad_polmax    = arad_polmax(0)
arad_Darkmax   = arad_Darkmax(0)
alec           = alec(0)
alesi          = alesi(0)
arad_polmin_aC = arad_polmin_aC(0)
arad_polmin_Si = arad_polmin_Si(0)

; set up radii:
narad          = 166
rr             = 6.e-7* 1.05^ findgen(narad)
alec           = alec            > min(rr)
alec           = alec            < max(rr)
alesi          = alesi           < max(rr)
alesi          = alesi           > min(rr)
arad_polmin_aC = arad_polmin_aC  > min(rr)
arad_polmin_Si = arad_polmin_Si  > min(rr)
arad_polmin_aC = arad_polmin_aC  < alec/1.05
arad_polmin_Si = arad_polmin_Si  < alesi/1.05

;
para(5) = alec        
para(6) = alesi       
para(7) = arad_polmin_aC
para(8) = arad_polmin_Si
; check arad_polmin_* le alesi:
if para(7) ge para(5)/1.049  or para(8) ge para(6)/1.049 then begin
   print, ' arad_polmin_aC, alec '
   print,   arad_polmin_aC, alec
   print, ' arad_polmin_Si, alesi '
   print,   arad_polmin_Si, alesi
   print,   para(5:8)
   stop, ' arad_polmin* gt alec or alesisi ?'
endif

;
; ---------------------------------------------
; alec, alesi as fixed parameters:

 get_lun, iu
  openw,iu,'./Input/jsm12fit.inp'
printf, iu, '  abuc    abusi    avsi   avgr    apah     qmrn  rlec      rlesi     r_pmin_aC r_pmin_Si r_pmax    r_Darkmax '
printf, iu, format='(6f8.3, 6e10.3)', abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, $
        alec, alesi, arad_polmin_aC, arad_polmin_Si, arad_polmax, arad_Darkmax 
  close, iu
free_lun, iu

spawn,   './a.crossec_dd >  message.out'

; Reddening curve of model for radii alec, alesi
 readcol, './Output/tau4fit.out', wr, taum, reddm, absredd, skipline=1, /sil


  if n_elements(wdata) eq nr    then  yfit = [absredd]     ; only redd fit
  
  return, yfit




end

; ===================================================================================================

Function fkt_absreddrad, wdata, parad

; RSI 28.11.2023
; help function for mpfit, 3 raddi as fit parameters: lec, lesi, led to fit absulte reddening
; ------------------------------------------
;  
;
; reads previous parameters: alec and alesi
 readcol, './Input/jsm12fit.inp', /sil, skipline=1, numli=1,  $
  abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, alec,  alesi, arad_polmin_aC, arad_polmin_Si, arad_polmax, aled



  narad             = 135
  rarad             = 1.502e-5 * 1.05^ findgen(narad)  < 0.010375
 lecsimax           = 18   ; entspricht max grainn size in d.Q of 3.4422083e-5
 
lec      = (nint(parad(0)) < lecsimax)  > 0
lesi     = (nint(parad(1)) < lecsimax)  > 0
led      = (nint(parad(2)) < (narad-1)) > 0


alec     = rarad(lec)    < 3.4420e-05
alesi    = rarad(lesi)   < 3.4420e-05
aled     = rarad(led)    < 1.0375e-02  


;print, format = '(a25, i4,1e9.2,4x, i4,1e9.2,4x, i4,1e9.2)', 'fkt_jsmRad: aC,Si,Dark : ', lec, alec, lesi, alesi, led, aled

arad_polmax  = aled

 get_lun, iu
  openw,iu,'./Input/jsm12fit.inp'
printf, iu, '  abuc    abusi    avsi   avgr    apah     qmrn  rlec      rlesi     r_pmin_aC r_pmin_Si r_pmax    r_Darkmax '
printf, iu, format='(6f8.3, 6e10.3)', abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, $
        alec, alesi, arad_polmin_aC, arad_polmin_Si, arad_polmax, aled
  close, iu
free_lun, iu


spawn,   './a.crossec_dd >  message.out'

; Reddening curve of model for radii alec, alesi
readcol, './Output/tau4fit.out', wr, taum, reddm, absredd, skipline=1, /sil



yfit = [absredd]                  ; absolure  redd E(w-V) fit



  return, yfit
end

; ===================================================================================================

Pro mp_absredd, ps=ps, no_mpfit=no_mpfit
  if not keyword_set(no_mpfit) then      no_mpfit = 0

window, 0, xsi=700, ysiz=700
   !p.thick=2
   !p.charsize =1.3
   !P.font = 7
 if keyword_set(ps) then begin
    !P.font = 0
    a = PSWINDOW(/cm)
    device, decomposed=0
    set_plot, 'PS'
    device, filename='idl.ps',BITS_PER_PIXEL=8, /color,  $
           XSIZE=a.xsize, YSIZE=a.ysize, XOFFSET=a.xoffset, YOFFSET=a.yoffset
 endif
  narad             = 135
  rarad             = 1.502e-5 * 1.05^ findgen(narad)  < 0.010375

  redchi2     = 999.

; PAH Drude parameters:
  x0org  = 4.6
  gamorg = 1.0

; restore Tab.1 entries
  restore, /verb, './Input/mk_dist.xdr'
   vrv_ref  = rv                ; Rv  of Extinction paper
   aV_ref   = av   > 0.001      ; Av  of Extinction paper
   delvar,  av, rv

; ---------------------------  Fit over all stars :
;
   
nn    = n_elements(star)
nn2   = 0 & nn3 = 0
for i = 0, nn-1 do begin


; either for testing only 1 star is used or all stars, comment next lines respectively
   
;   target  = 'HD287150'         ; just use one star
  target  = star(i)       ; just use one star if you wish to fit all stars

   if target eq star(i) then begin


print
 print, ' ================= START target = ', target, '   ================ '
 print

  finp = findfile('./StartParameters/'+target+'_jsm12fit.inp', cou=nfi)
  fpah = findfile('./StartParameters/'+target+'_PAH2170.wq',   cou=nfp)
  fxdr = findfile('./StartParameters/'+target+'_para.xdr',   cou=nf)

  
     if nf ne 1 or nfi ne 1 or nfp ne 1 then $
     stop, 'check files =', fext, finp, fpah, fxdr
     finp = finp(0) & fpah = fpah(0) & fxdr = fxdr(0)
     
; cp input files:     
                  comm = 'cp ' +finp(0)  + ' ./Input/jsm12fit.inp' 
           spawn, comm
; print, '1:   ', comm
                  comm = 'cp ' +fpah     + ' ./Input/PAH2170.wq'
                  spawn, comm
; print, '2:   ', comm

; read parameters and reddening curve from idl saveset file fxdr:
  restore, fxdr
  
   ebv_ref = av_ref(i) /vrv_ref(i) ; Ebv of reddening curve paper
   model   = reddpolfit(0:nr-1)
   data    = data(0:nr-1)   * ebv_ref
   edata   = edata(0:nr-1)  * ebv_ref
   wdata   = wdata(0:nr-1)

; save file jsmTaufit.inp which will be read by a.crossec_dd   
 get_lun, iu
 openw,iu,'./Input/jsmTaufit.inp'
 printf, iu, ' tauV, ebv = '
 printf,iu, tauV, ebv_ref
 close, iu
 free_lun, iu
   
readcol, './Input/jsm12fit.inp', /sil, skipline=1, numli=1,  $
          abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, alec,  alesi, $
          arad_polmin_aC, arad_polmin_Si, arad_polmax, aled 
 abusi   = abusi(0)*1d0
 aled    = aled(0)*1d0

 print, ' Start with input parameters used by a.crossec_dd'
 print, 'abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, alec, alesi, arad_polmin_aC, arad_polmin_Si, arad_polmax, aled, x0, gam'
 
 print, format='(6f8.3, 8e10.3)', abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, $
        alec, alesi, arad_polmin_aC, arad_polmin_Si, arad_polmax, aled, x0, gam  

; 
; ---------------------- now call MPFIT 
; a) first by keeping radii free and abundances fixed then
; b) second   keep radii fixed and abundances free
; c) iterate
 
  xtol = 1d-4
 for iter = 0, 1 do begin
 print
 print, '  MP1: free: lec, lesi, led  fixed: rest          at iter = ', iter
 print
 delvar, paRadius, parad
  paRadius = replicate({fixed:0, limited:[1,1], limits:[1,narad-1],mpside:2, step:1},3)
  paRadius(0).limits(0)  = 1.
  paRadius(0).limits(1)  = 18.
  paRadius(1).limits(0)  = 1.
  paRadius(1).limits(1)  = 18.
  paRadius(2).limits(0)  = 1.
  paRadius(2).limits(1)  = float(narad)-1.

  alec                   = alec(0)    < 3.4420d-05
  alesi                  = alesi(0)   < 3.4420d-05
  aled                   = aled(0)    < 1.0375d-02  
  lec                    = (where (rarad ge alec))(0)
  lesi                   = (where (rarad ge alesi))(0)
  led                    = (where (rarad ge aled))(0)
  parad         = float([lec,lesi,led])
  parad(0)      = ((parad(0) > paradius(0).limits(0)) < paradius(0).limits(1)) 
  parad(1)      = ((parad(1) > paradius(1).limits(0)) < paradius(1).limits(1))
  parad(2)      = ((parad(2) > paradius(2).limits(0)) < paradius(2).limits(1))
    


  if no_mpfit ge 1 then begin
     res = parad
  endif else begin
  stat = -1
  delvar, res
  res  = MPFITFUN('fkt_absreddRad', wdata(0:nr-1), data(0:nr-1), edata(0:nr-1), $
                 parad, PARINFO=paradius, perror=rms_para, status=stat,    $
                  yfit=reddfit, bestnorm=chi2, dof=dof, xtol = 1.)
  redchi2     = chi2/dof
endelse
  

  alec        = rarad(nint(res(0))) < 3.4420d-05
  alesi       = rarad(nint(res(1))) < 3.4420d-05
  aled        = rarad(nint(res(2))) < 1.0375d-02 
  alec        = alec(0)   &   alesi   = alesi(0)  &   aled  = aled(0)

  get_lun, iu
  openw,iu,'./Input/jsm12fit.inp'
printf, iu, '  abuc    abusi    avsi   avgr    apah     qmrn  rlec      rlesi     r_pmin_aC r_pmin_Si r_pmax    r_Darkmax '
printf, iu, format='(6f8.3, 6e10.3)', abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, $
        alec, alesi, arad_polmin_aC, arad_polmin_Si, arad_polmax, aled 
  close, iu
  free_lun, iu


  print, format='(6f8.3, 8e10.3)', abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, $
        alec, alesi, arad_polmin_aC, arad_polmin_Si, arad_polmax, aled, x0, gam 
; ---------------------

  print
  print, '  MP2 :  with 12 parameters:'

;  print, ' 0        1        2      3       4        5     6         7         8         9         10        11       '
;print, '  abuc    abusi    avsi   avgr    apah     qmrn  rlec      rlesi     r_pmin_aC r_pmin_Si r_pmax    r_Darkmax '
   

  alec        = alec(0)   &   alesi   = alesi(0)  &   aled   = aled(0) 
  para        = [abuc(0), abuvsi(0), abucvgr(0), abucpahs(0), qmrn(0),    $
                 alec(0), alesi(0), arad_polmin_aC(0), arad_polmin_Si(0), $
                 x0, gam]  * 1.d0
  


delvar, parain
parain = replicate({fixed:0, limited:[1,1], limits:[0.D,0.D],mpside:2},11)
parain(0).limits(0) =   0.1d0
parain(0).limits(1) = 160.0d0
parain(1).limits(0) =   1.0d-2
parain(1).limits(1) =  20.0d0
parain(2).limits(0) =   1.0d-2
parain(2).limits(1) =  40.0d0
parain(3).limits(0) =   1.0d-2
parain(3).limits(1) =  40.0d0
parain(4).limits(0) =   1.d0         ; qmrn
parain(4).limits(1) =   4.d0         ; 
parain(5).limits(0) =  min(rarad)    ; alec
parain(5).limits(1) =  3.4422083e-5
parain(6).limits(0) =  min(rarad)    ; alesi
parain(6).limits(1) =  3.4422083e-5  ; 
parain(7).limits(0) =  5d-6          ; arad_polmin_aC
parain(7).limits(1) =  3.4422083e-5  ; 
parain(8).limits(0) =  5d-6          ; arad_polmin_Si
parain(8).limits(1) =  3.4422083e-5  ; 
parain(9).limits(0) =  4.3           ; PAH x0
parain(9).limits(1) =  4.9
parain(10).limits(0)=  0.5           ; PAH gam
parain(10).limits(1)=  2.0


print, '  MP2: free: abundance+qmrn, fixed: radii         at iter = ', iter
print

   parain.fixed      = 0
   parain(5:8).fixed = 1
   save, filename='./Input/tmp_parain.xdr', parain, nr, np

  if no_mpfit ge 1 then begin
     res = para
  endif else begin
   stat = -1 
   delvar, res
   res  = MPFITFUN('fkt_absredd', wdata(0:nr-1), data(0:nr-1), edata(0:nr-1), $
                 para, PARINFO=parain, perror=rms_para, status=stat,    $
                 yfit=reddfit, bestnorm=chi2, dof=dof, xtol=xtol)
   para           = res
   
   rmsabuc           = rms_para(0)
   rmsabuvsi         = rms_para(1)
   rmsabucvgr        = rms_para(2)
   rmsabucpahs       = rms_para(3)
   rmsqmrn           = rms_para(4)
   rmsalec           = rms_para(5)
   rmsalesi          = rms_para(6)
   rmsarad_polmin_aC = rms_para(7)
   rmsarad_polmin_Si = rms_para(8)
   rmsx0             = rms_para(9)
   rmsgam            = rms_para(10)   
  endelse
  
   para(0)        = ((para(0) > parain(0).limits(0)) < parain(0).limits(1)) 
   para(1)        = ((para(1) > parain(1).limits(0)) < parain(1).limits(1)) 
   para(2)        = ((para(2) > parain(2).limits(0)) < parain(2).limits(1)) 
   para(3)        = ((para(3) > parain(3).limits(0)) < parain(3).limits(1)) 
   para(4)        = ((para(4) > parain(4).limits(0)) < parain(4).limits(1))
   para(5)        = ((para(5) > parain(5).limits(0)) < parain(5).limits(1))
   para(6)        = ((para(6) > parain(6).limits(0)) < parain(6).limits(1)) 
   para(7)        = ((para(7) > parain(7).limits(0)) < parain(7).limits(1))
   para(8)        = ((para(8) > parain(8).limits(0)) < parain(8).limits(1)) 
   para(7)        =   para(7) < para(5)
   para(8)        =   para(8) < para(6)
   para(9)        = ((para(9) > parain( 9).limits(0)) < parain( 9).limits(1))
   para(10)       = ((para(10)> parain(10).limits(0)) < parain(10).limits(1)) 

   abuc           =   para(0)
   abusi          =   abusi(0) *1d0
   abuvsi         =   para(1)
   abucvgr        =   para(2)
   abucpahs        =  para(3)
   qmrn           =   para(4)
   alec           =   para(5)
   alesi          =   para(6)
   arad_polmin_aC =   para(7)
   arad_polmin_Si =   para(8)   
   x0             =   para(9)
   gam            =   para(10)
   

get_lun, iu
openw,iu,'./Input/jsm12fit.inp'
printf, iu, '  abuc    abusi    avsi   avgr    apah     qmrn  rlec      rlesi     r_pmin_aC r_pmin_Si r_pmax    r_Darkmax '
printf, iu, format='(6f8.3, 6e10.3)', abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, $
        alec, alesi, arad_polmin_aC, arad_polmin_Si, arad_polmax, aled 
close, iu
free_lun, iu

; use org Drude parameters not those form redd fit !
   get_lun, iu
   openw,iu,'./Input/PAH2170.wq'
   printf, iu, 'x0 (mu)    gam : use ORG Drude Parameters of PAH at 2170 AA bump'
   printf,iu, x0, gam          ; change if ext papra shall be used
   printf,iu, x0org, gamorg    ; NOT used as OLD parameters as in ref paper 
   close, iu
   free_lun, iu
   

  print, format='(6f8.3, 8e10.3)', abuc, abusi, abuvsi, abucvgr, abucpahs, qmrn, $
        alec, alesi, arad_polmin_aC, arad_polmin_Si, arad_polmax, aled, x0, gam

  print

endfor
;
 print
 print, ' ----------------------------  MPfit is done   -------------------'
 print

; ===============================
; Save Fit results MPFIT results :
; ===============================

      

spawn,   './a.crossec_dd | tee  message.out'


res      = -9
spawn, 'grep Nd/Nl message.out', res
ratio_dl = float(strmid(res, 85,12))
NDust_d  = float(strmid(res, 61,12))
NDust_l  = float(strmid(res, 74,12))
ratio_dl = ratio_dl(0)  & NDust_d  = NDust_d(0) & NDust_l  = NDust_l(0)
if abs(1.-ndust_d/ndust_l/ratio_dl) gt 0.005 then stop, ' check string processing for nd,nl'

; -------------------------  PLOT result ---------


; Kappa.out
rdfloat, './Output/Kappa.out', skipline=3, w, sac, ssac, sasi, $
        sssi, savgr, ssvgr, savsi, ssvsi, sapah, sad, ssd, st, /sil
  w   = w*1e4
  iv  = (where(w le 0.548))(0) & ib  = (where(w le 0.445))(0)
  Rv_mod      = st(iv) / (st(ib) - st(iv))
 Ebv_mod      = 2.5/alog(10.) * (st(ib) - st(iv))
  Av_mod      = Rv_mod * Ebv_mod
 absredd_mod  = 2.5/alog(10.) * (st/st(iv) -1.) * st(iv) 
 Rv_ref       = vRv_ref(i)
 print, ' w(iv), w(ib)     : ',  w(iv), w(ib)
 print, ' Ebv_ref, Ebv_mod : ', ebv_ref, ebv_mod
 print, ' Rv_ref,  Rv_mod  : ',  Rv_ref,  Rv_mod


    salec  = strmid(string(nint(alec*1e7)),  5, 7) 
    salesi = strmid(string(nint(alesi*1e7)), 5, 7)
    saled  = strmid(string(nint(aled*1e7)),  4, 7) 
    spc    = strmid(string(nint(arad_polmin_aC*1e7)), 5, 7) 
    spsi   = strmid(string(nint(arad_polmin_si*1e7)), 5, 7) 

    tit    = 'c2=' + strmid(string(nint(100*redchi2)/100.),4,4)   + $
             ' R!DC!N= '  + salec+ ' R!DSi!N= ' + salesi + ' R!DD!N= ' + saled + $
             ' Rv:' +strmid(Rv_mod,6,4)+' d/l='+strmid(ratio_dl,5,4)
    
    yrange = [min([data-2.*edata])-0.2, max(data+edata)+0.2]

 plot, 1./w, absredd_mod, xrange=[-0.1,11.1], yrange=yrange, /xsty, /ysty, $
       xthick=4, ythick=4, /nodata
 
 oplot, 1./w, ((sac+ssac + sasi+sssi)/st(iv)   -1.)* Av_mod, color=fsc_color('brown')
 oplot, 1./w, ((savsi+ssvsi+ sapah+savgr+ssvgr)/st(iv) -1.)* Av_mod, color=fsc_color('forest green')
 oplot, 1./w, ((sad+ssd)/st(iv)     -1.)            * Av_mod, thick=!P.thick+1
 oplot, 1./w, absredd_mod, thick=4, color=fsc_color('magenta')

; print, 'Rv_ref, Rv_mod, Rv_obs, tauV, Ebv_ref, Ebv_mod'
; print,Rv_ref, Rv_mod, Rv_obs, tauV, Ebv_ref, Ebv_mod
; print, 'data(0), absredd_mod(0), Av_mod, tauV, ebv'
; print, data(0), absredd_mod(0), Av_mod, tauV, ebv
; stop, 'check Rv: '

; data
 oploterror, 1./wdata, data, wdata*0, 0.1*abs(data) > 0.05, /nohat, $
             color=fsc_color('dark gray'), psym=3
 oplot, 1./wdata(1:*), data(1:*),  psym=plotsym(/circle,scale=1.01,color=fsc_color('gray'), /fill)
 oplot, 1./wdata(1:*), data(1:*),  psym=plotsym(/circle,scale=1.01)
; model if tauV modified from Dgaia:
 oplot, 1./wdata(0) *[1,1], [data(0), data(0)],  psym=plotsym(/box,scale=1.09,color=fsc_color('Charcoal'), /fill)
 oplot, 1./wdata(0) *[1,1], [data(0), data(0)],  psym=plotsym(/box,scale=1.1)

 
 oplot, 1./w, absredd_mod, thick=1, color=fsc_color('magenta')
 xyouts, 1, max([data*0.85]), target, charsize=1.5


; print, ' before save -- data(0:3) = ', data(0:3)

 
; ======================
; Save Fit results 
; =================



data = data / ebv_ref           ; save relative reddening


print, ' in xdr save file =', './Result/'+ target+'_para.xdr' 
print, ' relative reddening E(x-V)/Ebv is saved'

    save, /xdr, filename='./Result/'+ target+'_para.xdr', $
      target, redchi2, redchi2p, $ 
      abuc, abusi,  abuvsi,    abucvgr,    abucpahs,  qmrn, alec, alesi, aled, $
      arad_polmin_aC, arad_polmin_Si, arad_polmax, x0, gam, $
       Ndust_l, Ndust_d, ratio_dl, Rv_ref, Rv_mod, Rv_obs, tauV, Ebv_ref, Ebv_mod, $
      rmsabuc, rmsabuSi, rmsabuvsi, rmsabucvgr, rmsabucpahs, rmsqmrn, $
      rmsalec, rmsalesi, rmsarad_polmin_aC,rmsarad_polmin_Si, rmsX0, rmsGam, $
      wdata, data, edata, edata_norm, reddpolfit, nr, np, $ 
      w, sac, ssac, sasi, sssi, savgr, ssvgr, savsi, ssvsi, sapah, sad, ssd, st
    
 
 spawn, 'cp ./Output/Kappa.out    ./Result/'+ target+'_Kappa.out'
 spawn, 'cp ./Output/PolKappa.out ./Result/'+ target+'_PolKappa.out'
 spawn, 'cp ./Input/jsm12fit.inp  ./Result/'+ target+'_jsm12fit.inp'
 spawn, 'cp ./Input/PAH2170.wq    ./Result/'+ target+'_PAH2170.wq'
 spawn, 'mv message.out           ./Result/'+ target+'_message.out'


endif        ; targte=star(i)
endfor          ; loop over all stars
   


if keyword_set(ps) then begin
   device, /close
   set_plot, 'X'
   device, decomposed=1
   print
   print
   print, ' Result of reddening fit in plot :  pl_allRedd.pdf'
   com =  ' ps2pdf                  idl.ps     pl_allRedd.pdf'
   spawn, com
   print, com + '     ->      DONE'
endif

print
print, ' mp_absredd: DONE'
print

end
