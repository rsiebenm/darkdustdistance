Pro pl_Fig1
; plot luminosity distance versus GAIA
; V=dwarfs, IV=subgiants, III=giants, II=bright giants, I=hyper giants
; Comments : 47 stars
; ------
;

!p.font=0
!p.charsize =1.3
!p.thick =2
    window, 0, xsi=500, ysize=600
    a = PSWINDOW(/cm)
    device, decomposed=0
    set_plot, 'PS'
    device, filename='idl.ps',BITS_PER_PIXEL=8, /color,  $
           XSIZE=a.xsize, YSIZE=a.ysize, XOFFSET=a.xoffset, YOFFSET=a.yoffset



restore, /verb, './Input/mk_dist.xdr'
nn = n_elements(ds)
rsg = ds/dg

ds_err0 = abs(ds - ds0)
ds_err1 = abs(ds - ds1)


;  --------  all stars --------
;
iiu    = where(rsg ge 1.)
iil    = where(rsg le 1.) 

;  --------  Only dwarfs lc =5 V stars  --------
;
ii5    = where(lc eq 5 or lc eq 4, nn5)
ii5_b  = where(lc eq 5 or lc eq 4 and sc ge 10 and sc lt 14,nn5_B ) 
ii5_b4 = where(lc eq 5 or lc eq 4 and sc ge 14,nn5_B4 ) 
ii5_o  = where(lc eq 5 or lc eq 4 and sc lt 10,nn5_O ) 

ii3    = where(lc eq 3, nn3) & print, nn5
ii3_b  = where(lc eq 3 and sc ge 10 and sc lt 14,nn3_B ) 
ii3_b4 = where(lc eq 3 and sc ge 14,nn3_B4 ) 
ii3_o  = where(lc eq 3 and sc lt 10,nn3_O ) 
; 2sig detections:
ii3_b_2s  = where(lc eq 3 and sc ge 10 and sc lt 14 and (rsg ge 1 and ( (ds-2.*abs(ds-ds0))/dg) ge 1),nn3_B_2s ) 
ii3_b4_2s = where(lc eq 3 and sc ge 14 and (rsg ge 1 and ( (ds-2.*abs(ds-ds0))/dg) ge 1),nn3_B4_2s ) 
ii3_o_2s  = where(lc eq 3 and sc lt 10 and (rsg ge 1 and ( (ds-2.*abs(ds-ds0))/dg) ge 1),nn3_O_2s ) 

ii1    = where(lc gt 5, nn1)
ii1_b  = where(lc gt 5 and sc ge 10 and sc lt 14,nn1_B ) 
ii1_b4 = where(lc gt 5 and sc ge 14,nn1_B4 ) 
ii1_o  = where(lc gt 5 and sc lt 10,nn1_O )

ii1_b_2s  = where(lc gt 5 and sc ge 10 and sc lt 14 and (rsg ge 1 and ( (ds-2.*abs(ds-ds0))/dg) ge 1),nn1_B_2s ) 
ii1_b4_2s = where(lc gt 5 and sc ge 14 and (rsg ge 1 and ( (ds-2.*abs(ds-ds0))/dg) ge 1),nn1_B4_2s ) 
ii1_o_2s  = where(lc gt 5 and sc lt 10 and (rsg ge 1 and ( (ds-2.*abs(ds-ds0))/dg) ge 1),nn1_O_2s ) 


; 3sig detections:
ii5_b_3s  = where(lc eq 5 or lc eq 4 and sc ge 10 and sc lt 14 and (rsg ge 1 and ( (ds-3.*abs(ds-ds0))/dg) ge 1),nn5_B_3s ) 
ii5_b4_3s = where(lc eq 5 or lc eq 4 and sc ge 14 and (rsg ge 1 and ( (ds-3.*abs(ds-ds0))/dg) ge 1),nn5_B4_3s ) 
ii5_o_3s  = where(lc eq 5 or lc eq 4 and sc lt 10 and (rsg ge 1 and ( (ds-3.*abs(ds-ds0))/dg) ge 1),nn5_O_3s ) 

ii3_b_3s  = where(lc eq 3 and sc ge 10 and sc lt 14 and (rsg ge 1 and ( (ds-3.*abs(ds-ds0))/dg) ge 1),nn3_B_3s ) 
ii3_b4_3s = where(lc eq 3 and sc ge 14 and (rsg ge 1 and ( (ds-3.*abs(ds-ds0))/dg) ge 1),nn3_B4_3s ) 
ii3_o_3s  = where(lc eq 3 and sc lt 10 and (rsg ge 1 and ( (ds-3.*abs(ds-ds0))/dg) ge 1),nn3_O_3s ) 


ii1_b_3s  = where(lc gt 5 and sc ge 10 and sc lt 14 and (rsg ge 1 and ( (ds-3.*abs(ds-ds0))/dg) ge 1),nn1_B_3s ) 
ii1_b4_3s = where(lc gt 5 and sc ge 14 and (rsg ge 1 and ( (ds-3.*abs(ds-ds0))/dg) ge 1),nn1_B4_3s ) 
ii1_o_3s  = where(lc gt 5 and sc lt 10 and (rsg ge 1 and ( (ds-3.*abs(ds-ds0))/dg) ge 1),nn1_O_3s ) 


; check number of stars:
;print, '  V stars: ', nn5_o+nn5_b+nn5_b4, nn5
;print, 'III stars: ', nn3_o+nn3_b+nn3_b4, nn3
;print, '  I stars: ', nn1_o+nn1_b+nn1_b4, nn1
print,' all stars: ', nn5+nn3+nn1, nn


; ------------  Ratio
plot, dg, rsg,  psym=4,  /nodata, $
      xr=[0, 2600.], yr=[0.5, 3], /xsty,/ysty, xthick=4, ythick=4, $
      xtitle = ' D!DGAIA!N (pc)', ytitle = ' D!DL!N / D!DGAIA!N '

polyfill, [0.,0.,2600., 2600.], [1.05, 1.37, 1.37, 1.05], Color=FSC_Color('light cyan')
oplot, [0., 2600], [1.37, 1.37], thick=2,color=fsc_color('gray')
oplot, [0., 2600], [1.05, 1.05], thick=2, color=fsc_color('gray')

xyouts, 2400, 0.98, '25%', charsize =0.7
xyouts, 2400, 1.39, '75%', charsize =0.7

; error bars:
for i =0, nn-1 do if(sc(i) ge 10 and sc(i) lt 14) then oplot, [dg(i), dg(i)], [ds0(i), ds1(i)]/dg(i), thick=1, color=fsc_color('blue')
for i =0, nn-1 do if(sc(i) lt 10) then oplot, [dg(i), dg(i)], [ds0(i), ds1(i)]/dg(i), thick=1, color=fsc_color('orange')
for i =0, nn-1 do if(sc(i) ge 14) then oplot, [dg(i), dg(i)], [ds0(i), ds1(i)]/dg(i), thick=1, color=fsc_color('green')
oploterror, dg, rsg, edg, rsg*0., psym=3, /nohat, thick=1, color=fsc_color('gray')


oplot, dg(ii5_b),  ds(ii5_b)/dg(ii5_b),    psym=plotsym(/superstar, /fill, scale=1.0013, color=fsc_color('blue'))

oplot, dg(ii5_o),  ds(ii5_o)/dg(ii5_o),    psym=plotsym(/superstar, /fill, scale=1.0013, color=fsc_color('orange'))

oplot, dg(ii5_b4), ds(ii5_b4)/dg(ii5_b4),  psym=plotsym(/superstar, /fill, scale=1.0013, color=fsc_color('green'))

oplot, dg(ii1_b), ds(ii1_b)/dg(ii1_b), psym=plotsym(/box, /fill, scale=1.0013, color=fsc_color('blue'))
oplot, dg(ii1_o), ds(ii1_o)/dg(ii1_o), psym=plotsym(/box, /fill, scale=1.0013, color=fsc_color('orange'))


oplot, dg(ii3_b),  ds(ii3_b)/dg(ii3_b),    psym=plotsym(/circle, /fill, scale=1.0013, color=fsc_color('blue'))
oplot, dg(ii3_o),  ds(ii3_o)/dg(ii3_o),    psym=plotsym(/circle, /fill, scale=1.0013, color=fsc_color('orange'))

oplot, dg(ii3_b4), ds(ii3_b4)/dg(ii3_b4),  psym=plotsym(/circle, /fill, scale=1.0013, color=fsc_color('green'))

oplot, dg(ii1), ds(ii1)/dg(ii1), psym=plotsym(/box, scale=1.0013)
oplot, dg(ii3), ds(ii3)/dg(ii3), psym=plotsym(/circle, scale=1.0013)
oplot, dg(ii5), ds(ii5)/dg(ii5), psym=plotsym(/superstar, scale=1.0013)


; 3 sigma:

if nn5_O_3s  gt 0 then oplot, dg(ii5_o_3s),  ds(ii5_o_3s)/dg(ii5_o_3s),    psym=plotsym(/superstar, /fill, scale=1.95, color=fsc_color('orange'))
if nn5_B_3s  gt 0 then oplot, dg(ii5_b_3s),  ds(ii5_b_3s)/dg(ii5_b_3s),    psym=plotsym(/superstar, /fill, scale=1.95, color=fsc_color('blue'))
if nn5_B4_3s gt 0 then oplot, dg(ii5_b4_3s), ds(ii5_b4_3s)/dg(ii5_b4_3s),  psym=plotsym(/superstar, /fill, scale=1.95, color=fsc_color('green'))

if nn3_O_3s  gt 0 then oplot, dg(ii3_o_3s),  ds(ii3_o_3s)/dg(ii3_o_3s),    psym=plotsym(/circle, /fill, scale=1.95, color=fsc_color('orange'))
if nn3_B_3s  gt 0 then oplot, dg(ii3_b_3s),  ds(ii3_b_3s)/dg(ii3_b_3s),    psym=plotsym(/circle, /fill, scale=1.95, color=fsc_color('blue'))
if nn3_B4_3s gt 0 then oplot, dg(ii3_b4_3s), ds(ii3_b4_3s)/dg(ii3_b4_3s),  psym=plotsym(/circle, /fill, scale=1.95, color=fsc_color('green'))

if nn1_O_3s  gt 0 then oplot, dg(ii1_o_3s),  ds(ii1_o_3s)/dg(ii1_o_3s),    psym=plotsym(/box, /fill, scale=1.95, color=fsc_color('orange'))
if nn1_B_3s  gt 0 then oplot, dg(ii1_b_3s),  ds(ii1_b_3s)/dg(ii1_b_3s),    psym=plotsym(/box, /fill, scale=1.95, color=fsc_color('blue'))

; rand:
if nn5_O_3s  gt 0 then oplot, dg(ii5_o_3s),  ds(ii5_o_3s)/dg(ii5_o_3s),    psym=plotsym(/superstar,  scale=1.95)
if nn5_B_3s  gt 0 then oplot, dg(ii5_b_3s),  ds(ii5_b_3s)/dg(ii5_b_3s),    psym=plotsym(/superstar,  scale=1.95)
if nn5_B4_3s gt 0 then oplot, dg(ii5_b4_3s), ds(ii5_b4_3s)/dg(ii5_b4_3s),  psym=plotsym(/superstar,  scale=1.95)

if nn3_O_3s  gt 0 then oplot, dg(ii3_o_3s),  ds(ii3_o_3s)/dg(ii3_o_3s),    psym=plotsym(/circle,  scale=1.95)
if nn3_B_3s  gt 0 then oplot, dg(ii3_b_3s),  ds(ii3_b_3s)/dg(ii3_b_3s),    psym=plotsym(/circle,  scale=1.95)
if nn3_B4_3s gt 0 then oplot, dg(ii3_b4_3s), ds(ii3_b4_3s)/dg(ii3_b4_3s),  psym=plotsym(/circle,  scale=1.95)

if nn1_O_3s  gt 0 then oplot, dg(ii1_o_3s),  ds(ii1_o_3s)/dg(ii1_o_3s),    psym=plotsym(/box,  scale=1.95)
if nn1_B_3s  gt 0 then oplot, dg(ii1_b_3s),  ds(ii1_b_3s)/dg(ii1_b_3s),    psym=plotsym(/box,  scale=1.95)



legend, ['O4  -  O9', 'B0  -  B3', 'B4  -  A0'], charsize=1.2, charthick=6, $
        textcolor=[fsc_color('orange'), fsc_color('blue'), fsc_color('green')]


x=0.03
oplot, [1920.,1920], [2.92-x,2.92-x], psym=plotsym(/box, scale=0.9)
oplot, [1920.,1920], [2.85-x,2.85-x], psym=plotsym(/circle, scale=0.9)
oplot, [1920.,1920], [2.78-x,2.78-x], psym=plotsym(/superstar, scale=0.9)


x=0.05
xyouts, 1970, 2.78 -x, 'Dwarfs V-IV', charsize=0.95
xyouts, 1970, 2.92 -x, 'Supergiants I', charsize=0.95
xyouts, 1970, 2.85 -x, 'Giants III', charsize=0.95

plot, dg, rsg, /nodata, /noerase, $
      xr=[0, 2600.], yr=[0.5, 3], /xsty,/ysty, xthick=4, ythick=4, $
      xtitle = ' D!DGAIA!N (pc)', ytitle = ' D!DL!N / D!DGAIA!N '


; --------------

   device, /close
   set_plot, 'X'
   device, decomposed=1
   print
   print, ' Result of reddening fit in plot :  pl_Fig1.pdf'
   com =  ' ps2pdf                  idl.ps     pl_Fig1.pdf'
spawn, com
print, com + '     ->      DONE'

end
